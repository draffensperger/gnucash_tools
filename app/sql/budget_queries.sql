
/* See all budget categories, use this to paste into our spreadsheet */
select
cast(xaction_accounts.name as text) as category,
cast(xaction_accounts.description as text) as area,
xaction_credit.numeric_val_num / (1.0 * xaction_credit.numeric_val_denom) -
xaction_debit.numeric_val_num / (1.0 * xaction_debit.numeric_val_denom) as amount,
cast(schedxactions.name as text) as pay_period
from transactions
inner join splits on splits.tx_guid = transactions.guid
inner join accounts on splits.account_guid = accounts.guid
inner join schedxactions on accounts.guid = schedxactions.template_act_guid
inner join slots as sched_xaction on sched_xaction.obj_guid = splits.guid
inner join slots as xaction_account_guid on sched_xaction.guid_val = xaction_account_guid.obj_guid and xaction_account_guid.name = 'sched-xaction/account'
inner join slots as xaction_credit on sched_xaction.guid_val = xaction_credit.obj_guid and xaction_credit.name = 'sched-xaction/credit-numeric'
inner join slots as xaction_debit on sched_xaction.guid_val = xaction_debit.obj_guid and xaction_debit.name = 'sched-xaction/debit-numeric'
inner join accounts as xaction_accounts on xaction_account_guid.guid_val = xaction_accounts.guid
where xaction_accounts.name <> 'Budget'
order by
case
when area like 'Tithe%' then 0
when area like 'Retirement%' then 1
when area like 'House Savings%' then 2
when area like 'Housing / Utilities%' then 3
when area like 'Transport%' then 4
when area like 'Household%' then 5
when area like 'Personal%' then 6
when area like 'Gifts%' then 7
else 99
end;


/* See paychecks for a certain date. */
select splits.value_num * (100 / splits.value_denom), transactions.post_date from
splits inner join accounts on splits.account_guid = accounts.guid
inner join transactions on transactions.guid = splits.tx_guid
where accounts.name = 'CCC Pay'
and post_date >= 20120309050000 and post_date <= 20120323050000;

/* See net budget */
select schedxactions.name,
sum (xaction_credit.numeric_val_num * (100/ xaction_credit.numeric_val_denom)) / -100.0 as total_credit,
(
sum (xaction_credit.numeric_val_num * (100/ xaction_credit.numeric_val_denom))
+
(select sum(splits.value_num * (100 / splits.value_denom)) from
splits inner join accounts on splits.account_guid = accounts.guid
inner join transactions on transactions.guid = splits.tx_guid
where accounts.name = 'CCC Pay'
and post_date = 20120309050000)
) / -100.0 as net_after_pay
from transactions
inner join splits on splits.tx_guid = transactions.guid
inner join accounts on splits.account_guid = accounts.guid
inner join schedxactions on accounts.guid = schedxactions.template_act_guid
inner join slots as sched_xaction on sched_xaction.obj_guid = splits.guid
inner join slots as xaction_account_guid on sched_xaction.guid_val = xaction_account_guid.obj_guid and xaction_account_guid.name = 'sched-xaction/account'
inner join slots as xaction_credit on sched_xaction.guid_val = xaction_credit.obj_guid and xaction_credit.name = 'sched-xaction/credit-numeric'
inner join slots as xaction_debit on sched_xaction.guid_val = xaction_debit.obj_guid and xaction_debit.name = 'sched-xaction/debit-numeric'
inner join accounts as xaction_accounts on xaction_account_guid.guid_val = xaction_accounts.guid
where xaction_accounts.name <> 'Budget'
group by schedxactions.name;