select
num,
(post_date / 10000000000) || '-' || ((post_date % 10000000000) / 100000000) || '-' || ((post_date % 100000000) / 1000000) as post_date,
abs((splits.quantity_num * 1.0) / (splits.quantity_denom * 1.0)) as amount
from transactions
inner join splits on splits.tx_guid = transactions.guid
inner join accounts on accounts.guid = splits.account_guid
where accounts.code = 'ReimbursementReceived'
order by transactions.post_date