select 

num, 
sum(amount) as total_unrounded,
sum(amount_rounded) as total,
(max(post_date) / 10000000000) || '-' || ((max(post_date) % 10000000000) / 100000000) || '-' || ((max(post_date) % 100000000) / 1000000) as max_post_date,
sum(num_receipts) as receipts

from (
	select
	num,
    post_date,
	post_date as trans_post_date,
	accounts.code as code,	
	case when commodities.mnemonic <> 'USD' then (value_num * 1.0) / (value_denom * 1.0) else (quantity_num * 1.0) / (quantity_denom * 1.0) end as amount,
	round(case when commodities.mnemonic <> 'USD' then (value_num * 1.0) / (value_denom * 1.0) else (quantity_num * 1.0) / (quantity_denom * 1.0) end, 0) as amount_rounded,
	case
        when accounts.code in ('MDB') then 1
        when accounts.code in ('miles') then 0
		when accounts.code in ('OldReimb') then 0
        when round(case when commodities.mnemonic <> 'USD' then (value_num * 1.0) / (value_denom * 1.0) else (quantity_num * 1.0) / (quantity_denom * 1.0) end, 0) > 75 then 1
        else 0
    end as num_receipts
	from transactions
	inner join splits on splits.tx_guid = transactions.guid
	inner join accounts on accounts.guid = splits.account_guid
	inner join accounts as parents on accounts.parent_guid = parents.guid
	inner join commodities on accounts.commodity_guid = commodities.guid	
	where (accounts.code in ('MEA', 'CHI', 'POS', 'PRI', 'TAR', 'MDC', 'LOD', 'SUP', 'MTG', 'MDA', 'TAU', 'ENT', 'TEL', 'MDB', 'miles', 'ZOldReimb')
	  or parents.code in ('MEA', 'CHI', 'POS', 'PRI', 'TAR', 'MDC', 'LOD', 'SUP', 'MTG', 'MDA', 'TAU', 'ENT', 'TEL', 'MDB', 'miles', 'ZOldReimb'))
)
group by num
having num <> ''
order by trans_post_date, num