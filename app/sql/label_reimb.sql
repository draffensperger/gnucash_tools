update transactions set num = {label}
where num = '' and
guid in
(select tx_guid from splits
inner join accounts on accounts.guid = splits.account_guid
inner join accounts as parents on parents.guid = accounts.parent_guid
where ({reimb_type} = 'Ministry' AND (accounts.code in ('MEA', 'CHI', 'POS', 'PRI', 'TAR', 'MDC', 'LOD', 'SUP', 'MTG', 'TAU', 'ENT', 'TEL', 'miles') or 
									  parents.code in ('MEA', 'CHI', 'POS', 'PRI', 'TAR', 'MDC', 'LOD', 'SUP', 'MTG', 'TAU', 'ENT', 'TEL', 'miles'))
or ({reimb_type} = 'Medical' AND (accounts.code in ('MDB', 'MDA') or parents.code in ('MDB', 'MDA')))
));