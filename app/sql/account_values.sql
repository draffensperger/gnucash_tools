select
accounts.name as name,
accounts.code as code,
accounts.account_type as account_type,
case when acct_group_info.acct_group is null then 'Ungrouped' else acct_group_info.acct_group end as acct_group,
acct_parents.name as parent_name,
sum(val) as acct_value
from
(
	select
	case
		when acct_equiv.guid_val <> '' then acct_equiv.guid_val
		else splits.account_guid
	end as acct_guid,
	(splits.quantity_num * 1.0) / (splits.quantity_denom * 1.0) as val
	from splits
	inner join transactions on transactions.guid = splits.tx_guid
	left join (select * from slots where name = 'acct_equiv') as acct_equiv
		on acct_equiv.obj_guid = splits.account_guid
) as combined_splits

inner join accounts on combined_splits.acct_guid = accounts.guid
inner join accounts as acct_parents on acct_parents.guid = accounts.parent_guid

left join (
	select distinct
	acct_groups.obj_guid as acct_guid, 
	acct_groups.string_val as acct_group, 
	acct_group_orders.int64_val as acct_group_order
	from (select * from slots where name = 'acct_group') as acct_groups
	inner join (select * from slots as acct_group_orders where acct_group_orders.name = 'acct_group_order') as acct_group_orders
		on acct_group_orders.string_val = acct_groups.string_val
) as acct_group_info
on acct_group_info.acct_guid = combined_splits.acct_guid

group by combined_splits.acct_guid
order by case when acct_group_info.acct_group is null then 9999 else acct_group_order end, accounts.name;