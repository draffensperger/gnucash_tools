select
num,
(post_date / 10000000000) || '-' || ((post_date % 10000000000) / 100000000) || '-' || ((post_date % 100000000) / 1000000) as post_date,
case when accounts.code = '' or accounts.code is null then parents.code else accounts.code end as code,
case when splits.memo <> '' then splits.memo else slots.string_val end as notes,
round((splits.quantity_num * 1.0) / (splits.quantity_denom * 1.0), 2) as amount
from transactions
inner join splits on splits.tx_guid = transactions.guid
inner join accounts on accounts.guid = splits.account_guid
inner join accounts as parents on accounts.parent_guid = parents.guid
left join slots on slots.obj_guid = transactions.guid
where (accounts.code in ('MEA', 'CHI', 'POS', 'PRI', 'TAR', 'MDC', 'LOD', 'SUP', 'MTG', 'MDA', 'TAU', 'ENT', 'TEL', 'MDB', 'miles')
or parents.code in ('MEA', 'CHI', 'POS', 'PRI', 'TAR', 'MDC', 'LOD', 'SUP', 'MTG', 'MDA', 'TAU', 'ENT', 'TEL', 'MDB', 'miles'))
and num = {num}
and slots.name = 'notes'
order by transactions.post_date
;