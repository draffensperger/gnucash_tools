select abs(sum(splits.value_num / splits.value_denom)) as net_pay, transactions.post_date from
splits inner join accounts on splits.account_guid = accounts.guid
inner join transactions on transactions.guid = splits.tx_guid
where accounts.name = 'CCC Pay'
group by post_date
order by post_date desc
limit 1;