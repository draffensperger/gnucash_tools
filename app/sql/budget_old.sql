select schedxactions.name,
sum (xaction_credit.numeric_val_num * (100/ xaction_credit.numeric_val_denom)) / -100.0 as total_credit,
(
sum (xaction_credit.numeric_val_num * (100/ xaction_credit.numeric_val_denom))
+
(select sum(splits.value_num * (100 / splits.value_denom)) from
splits inner join accounts on splits.account_guid = accounts.guid
inner join transactions on transactions.guid = splits.tx_guid
where accounts.name = 'CCC Pay'
and post_date = {payday})
) / -100.0 as net_after_pay
from transactions
inner join splits on splits.tx_guid = transactions.guid
inner join accounts on splits.account_guid = accounts.guid
inner join schedxactions on accounts.guid = schedxactions.template_act_guid
inner join slots as sched_xaction on sched_xaction.obj_guid = splits.guid
inner join slots as xaction_account_guid on sched_xaction.guid_val = xaction_account_guid.obj_guid and xaction_account_guid.name = 'sched-xaction/account'
inner join slots as xaction_credit on sched_xaction.guid_val = xaction_credit.obj_guid and xaction_credit.name = 'sched-xaction/credit-numeric'
inner join slots as xaction_debit on sched_xaction.guid_val = xaction_debit.obj_guid and xaction_debit.name = 'sched-xaction/debit-numeric'
inner join accounts as xaction_accounts on xaction_account_guid.guid_val = xaction_accounts.guid
where xaction_accounts.name <> 'Budget'
group by schedxactions.name;

/*
select * from recurrences;

c < {p1}
asdf
b={p2}

select splits.*
from splits
where splits.tx_guid in
(select transactions.guid
from transactions
inner join splits as s1 on s1.tx_guid = transactions.guid
inner join accounts on s1.account_guid = accounts.guid
inner join schedxactions
on accounts.guid = schedxactions.template_act_guid);

select * from splits;

select * from schedxactions ;

select schedxactions.name, xaction_accounts.name,
xaction_credit.numeric_val_num * (100 / xaction_credit.numeric_val_denom) as credit,
xaction_debit.numeric_val_num * (100 / xaction_debit.numeric_val_denom) as debit
from transactions
inner join splits on splits.tx_guid = transactions.guid
inner join accounts on splits.account_guid = accounts.guid
inner join schedxactions on accounts.guid = schedxactions.template_act_guid
inner join slots as sched_xaction on sched_xaction.obj_guid = splits.guid
inner join slots as xaction_account_guid on sched_xaction.guid_val = xaction_account_guid.obj_guid and xaction_account_guid.name = 'sched-xaction/account'
inner join slots as xaction_credit on sched_xaction.guid_val = xaction_credit.obj_guid and xaction_credit.name = 'sched-xaction/credit-numeric'
inner join slots as xaction_debit on sched_xaction.guid_val = xaction_debit.obj_guid and xaction_debit.name = 'sched-xaction/debit-numeric'
inner join accounts as xaction_accounts on xaction_account_guid.guid_val = xaction_accounts.guid
where xaction_accounts.name <> 'Budget';



select splits.value_num * (100 / splits.value_denom), transactions.post_date from
splits inner join accounts on splits.account_guid = accounts.guid
inner join transactions on transactions.guid = splits.tx_guid
where accounts.name = 'CCC Pay'
and post_date >= 20120309050000 and post_date <= 20120323050000;


select * from slots where name like 'sched%';

select * from accounts where parent_guid = (select guid from accounts where name = 'Template Root');
select * from accounts;

select guid from accounts where name = 'Template Root';
select * from accounts where guid = '2284aa9dae9de1aba52ae1db6fc19a84';
select * from accounts where guid = '4916f1ed6313f4cae5bc4d1b813bb466';
select * from transactions;
select * from splits where account_guid = '4916f1ed6313f4cae5bc4d1b813bb466';
*/