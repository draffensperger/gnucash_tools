select
cast(xaction_accounts.name as text) as name,
budget_groups.string_val as budget_group,
budget_group_order.int64_val as budget_group_order,
sum(xaction_credit.numeric_val_num / (1.0 * xaction_credit.numeric_val_denom) -
xaction_debit.numeric_val_num / (1.0 * xaction_debit.numeric_val_denom)) as amount
from transactions
inner join splits on splits.tx_guid = transactions.guid
inner join accounts on splits.account_guid = accounts.guid
inner join schedxactions on accounts.guid = schedxactions.template_act_guid
inner join slots as sched_xaction on sched_xaction.obj_guid = splits.guid
inner join slots as xaction_account_guid on sched_xaction.guid_val = xaction_account_guid.obj_guid and xaction_account_guid.name = 'sched-xaction/account'
inner join slots as xaction_credit on sched_xaction.guid_val = xaction_credit.obj_guid and xaction_credit.name = 'sched-xaction/credit-numeric'
inner join slots as xaction_debit on sched_xaction.guid_val = xaction_debit.obj_guid and xaction_debit.name = 'sched-xaction/debit-numeric'
inner join accounts as xaction_accounts on xaction_account_guid.guid_val = xaction_accounts.guid
inner join slots as budget_groups on xaction_account_guid.guid_val = budget_groups.obj_guid and budget_groups.name = 'budget_group'
inner join slots as budget_group_order on budget_group_order.string_val = budget_groups.string_val and budget_group_order.name = 'budget_group_order'
where xaction_accounts.name <> 'Budget'
group by name
order by budget_group_order, amount desc;