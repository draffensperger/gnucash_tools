// Helper functions 



function queryServer(baseUrl, params, callback) {	
	callServer(baseUrl, params, function(responseText) {
		callback(csvRowsWithHeaderToObject(parseCSV(responseText)))
	});
}


function formatPercent(x, dp) {
	return makeDpStr(x * 100.0, dp) + '%';
}

// From: http://stackoverflow.com/questions/2646385/add-a-thousands-separator-to-a-total-with-javascript-or-jquery
function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

var makeDpStr = function(numStr, dp) {
	return parseFloat(numStr).toFixed(dp);
}
var formatMoneyStr = function(str, dp) {		
	return formatMoneyFloat(parseFloat(str), dp);
}
var formatMoneyFloat = function(val, dp) {	
	if (dp == null) {
		dp = 2;
	}
	
	if (typeof val === 'undefined') {
		return '$[UNDEFINED]';
	}
	
	if (val < 0.0) {
		return "<span style='color:red'>$(" + addCommas((-val).toFixed(dp)) + ")</span>";
	} else {
		return "$" + addCommas(val.toFixed(dp));
	}	
}




// Taken from: http://en.wikipedia.org/wiki/XMLHttpRequest
// Based on findings at: http://blogs.msdn.com/xmlteam/archive/2006/10/23/using-the-right-version-of-msxml-in-internet-explorer.aspx
function getHTTPObject() {
	var http = false;
	if (typeof XMLHttpRequest == "undefined") {
		try { http = new ActiveXObject("Msxml2.XMLHTTP.6.0"); }
		  catch (e) {}
		try { http = new ActiveXObject("Msxml2.XMLHTTP.3.0"); }
		  catch (e) {}
		//Microsoft.XMLHTTP points to Msxml2.XMLHTTP and is redundant
		try { http = new ActiveXObject("Microsoft.XMLHTTP"); }
		  catch (e) {}
	} else if (window.XMLHttpRequest) {
		try {http = new XMLHttpRequest();}
			catch (e) {}
	}
	return http;
}

function callServer(baseUrl, params, callback) {
	var url = baseUrl;
	
	if (typeof params == "object") {
		var urlParamPart = "";
		var numParms;
		var field;
		for (field in params) {
			if (urlParamPart != "") {
				urlParamPart += "&";
			}
					
			if (field != "") {
				urlParamPart += field + '=' + encodeURIComponent(params[field]);
			}
		}
		
		if (urlParamPart != "") {
			url += "?" + urlParamPart;
		}
	}

	var xhttp = getHTTPObject();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4) {
			if (xhttp.status == 200) {
				callback(xhttp.responseText);
			} else {
				callback("HTTP ERROR STATUS: " + xhttp.status);
			}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send(null);
}
function parseCSV(csv) {
	var rows = [];
	var row = [];
	var data = "";
	
	var inQuotes = false;
	var wasQuote = false;	
	
	var numChars = csv.length;
	for (i = 0; i < numChars; i++) {	
		var c = csv[i];
		
		switch (c) {
		case ',':
			if (inQuotes) {
				if (wasQuote) {
					inQuotes = false;
					wasQuote = false;
					row.push(data);
					data = "";
				} else {
					data += c;
				}				
			} else {
				row.push(data);
				data = "";
			}
			break;
		case '"':
			if (inQuotes) {
				if (wasQuote) {
					data += c;
					wasQuote = false;
				} else {
					wasQuote = true;
				}
			} else {
				inQuotes = true;
			}			
			break;
		case '\n':
			if (inQuotes) {
				if (wasQuote) {
					inQuotes = false;
					wasQuote = false;
					row.push(data);
					data = "";
					rows.push(row);
					row = [];
				} else {
					data += c;
				}
			} else {
				row.push(data);
				data = "";
				rows.push(row);
				row = [];
			}
			break;
		default:
			data += c;
		}		
	}
	
	if (data != "" || rows.length > 0 || row.length > 0) {
		row[row.length] = data;
		rows[rows.length] = row;
	}
	
	return rows;
}

function csvRowsWithHeaderToObject(rows) {
	if (rows.length < 1) {
		return {};
	}
	
	var headerRow = rows[0];
		
	var rowObjs = [];
	
	var numRows = rows.length;
	for (var rowNum = 1; rowNum < numRows; rowNum++) {		
		var rowObj = {};
		var row = rows[rowNum];
		for (colNum in row) {
			rowObj[headerRow[colNum]] = row[colNum];			
		}
		rowObjs.push(rowObj);
	}
	
	return rowObjs;
}