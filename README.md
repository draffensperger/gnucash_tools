**Gnucash_tools** is a set of utilities for viewing and manipulating a [Gnucash](http://www.gnucash.org/) expense database in Sqlite format. The utilities are meant to compliment the functionality of GnuCash itself and I designed them specifically for my needs as a Cru staff member.

There are currently four utilities/reports included:

- **Reimbursements** allows you to view unreimbursed expenses, create a draft Cru reimbursement for them, and mark that as submitted. It also matches previously submitted reimbursements with bank deposits from Cru marked as reimbursement payments.
- **Account Balances** allows you to view the balances of budget categories values. They will be sorted according to some account meta data which is set by custom SQL queries (see `sample_data/account_meta_info.xlsx` for more). 
- **Budget** which shows a report based on GnuCash scheduled transactions to break down your monthly budget into different categories and view the totals at a glance even if the budget transactions occur twice per month (for each paycheck).
- **Paycheck Balancing** is a simple report to show the net amount leftover after each paycheck to help you balance the schedule transactions to budget the paychecks into different budget categories.

## Installation

### Install GnuCash

This isn't strictly a dependency for this project, but this is meant to be used in conjunction with the [GnuCash](http://www.gnucash.org/) application and integrates tightly with its data. If you are installing GnuCash on Linux, make sure you install/build support for the Sqlite data format. 

### Install Sqlite_httpserve

These tools are meant to be served locally from your computer using a custom web server called [sqlite_httpserve](https://github.com/draffensperger/sqlite_httpserve). Get the code first by running:

	git clone https://github.com/draffensperger/sqlite_httpserve.git 

Then follow the build instructions at the [sqlite_httpserve project page](https://github.com/draffensperger/sqlite_httpserve).

### Install Gnucash_tools

Run a similar command to get the code for gnucash_tools:

	git clone https://github.com/draffensperger/gnucash_tools

## Running gnucash_tools

To run gnucash\_tools, you will need to run sqlite\_httpserve against the Gnucash database in sqlite format. Assuming that both sqlite\_httpserve and gnu\_cash tools are in the same directory and you are in a terminal in the containing directory for both, you could run this command to view gnucash\_tools for the sample database:
	
	./sqlite_httpserve/bin/release/sqlite_httpserve 127.0.0.1 8000 gnucash_tools/app gnucash_tools/sample/sample.sqlite3.gnucash

Then to view the tools, open a web browswer to `http://127.0.0.1:8000/`. 

## License

The MIT License (MIT)

Copyright (c) 2014 David Raffensperger

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.